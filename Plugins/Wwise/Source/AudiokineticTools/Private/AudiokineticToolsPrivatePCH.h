// Copyright (c) 2006-2012 Audiokinetic Inc. / All Rights Reserved

/*=============================================================================
	AudiokineticToolsPrivatePCH.h:
=============================================================================*/
#pragma once

#include "IAudiokineticTools.h"
#include "AkUEFeatures.h"
#if !UE_4_15_OR_LATER
#include "UnrealEd.h"
#endif