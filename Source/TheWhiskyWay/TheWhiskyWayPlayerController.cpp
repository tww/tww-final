// Fill out your copyright notice in the Description page of Project Settings.

#include "TheWhiskyWay.h"
#include "TheWhiskyWayPlayerController.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

ATheWhiskyWayPlayerController::ATheWhiskyWayPlayerController(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{

};

void ATheWhiskyWayPlayerController::BeginPlay()
{
	GameInstance = Cast<UTheWhiskyWayGameInstance>(GetGameInstance());
}

void ATheWhiskyWayPlayerController::Tick(float DeltaSeconds)
{

}

void ATheWhiskyWayPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	check(InputComponent);

    bBlockInput = false;

	InputComponent->BindAxis("Turn", this, &ATheWhiskyWayPlayerController::YawInput);
	InputComponent->BindAxis("LookUp", this, &ATheWhiskyWayPlayerController::PitchInput);
	
	InputComponent->BindAxis("MoveForward", this, &ATheWhiskyWayPlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ATheWhiskyWayPlayerController::MoveRight);

	InputComponent->BindAction("Drink", IE_Released, this, &ATheWhiskyWayPlayerController::Drink);

	InputComponent->BindAction("Sprint", IE_Pressed, this, &ATheWhiskyWayPlayerController::Sprint);
	InputComponent->BindAction("Sprint", IE_Released, this, &ATheWhiskyWayPlayerController::Sprint);

	InputComponent->BindAction("Interact", IE_Pressed, this, &ATheWhiskyWayPlayerController::StartInteract);
	InputComponent->BindAction("Interact", IE_Released, this, &ATheWhiskyWayPlayerController::EndInteract);

	InputComponent->BindAction("ToggleRepairMode", IE_Pressed, this, &ATheWhiskyWayPlayerController::LMBPressed);
	InputComponent->BindAction("ToggleRepairMode", IE_Released, this, &ATheWhiskyWayPlayerController::LMBReleased);

	InputComponent->BindAction("Throw", IE_Pressed, this, &ATheWhiskyWayPlayerController::RMBPressed);

	InputComponent->BindAction("ToggleHacking", IE_Released, this, &ATheWhiskyWayPlayerController::EnterPressed);

	InputComponent->BindAction("QTE", IE_Pressed, this, &ATheWhiskyWayPlayerController::SpacePressed);
	InputComponent->BindAction("QTE", IE_Released, this, &ATheWhiskyWayPlayerController::SpaceReleased);

	InputComponent->BindAction("Hacking", IE_Released, this, &ATheWhiskyWayPlayerController::AnyKey);

    InputComponent->BindAction("Pause", IE_Pressed, this, &ATheWhiskyWayPlayerController::PausePressed).bExecuteWhenPaused = true;

	InputComponent->BindAction("Walk", IE_Pressed, this, &ATheWhiskyWayPlayerController::WalkPressed);
	InputComponent->BindAction("Walk", IE_Released, this, &ATheWhiskyWayPlayerController::WalkReleased);

}

void ATheWhiskyWayPlayerController::YawInput(float RotVal)
{
	GameInstance->GlobalEventHandler->OnInputAxisMappingEvent.Broadcast(InputEvents::Turn, RotVal);
}

void ATheWhiskyWayPlayerController::PitchInput(float RotVal)
{	
	GameInstance->GlobalEventHandler->OnInputAxisMappingEvent.Broadcast(InputEvents::LookUp, RotVal);
}

void ATheWhiskyWayPlayerController::MoveForward(float MoveVal)
{
	GameInstance->GlobalEventHandler->OnInputAxisMappingEvent.Broadcast(InputEvents::Forward, MoveVal);
}

void ATheWhiskyWayPlayerController::MoveRight(float MoveVal)
{
	GameInstance->GlobalEventHandler->OnInputAxisMappingEvent.Broadcast(InputEvents::Right, MoveVal);
}

void ATheWhiskyWayPlayerController::Drink(FKey Key)
{
	GameInstance->GlobalEventHandler->OnInputActionBindingEvent.Broadcast(InputEvents::Drink, Key);
}

void ATheWhiskyWayPlayerController::Sprint(FKey Key)
{
	GameInstance->GlobalEventHandler->OnInputActionBindingEvent.Broadcast(InputEvents::Sprint, Key);
}

void ATheWhiskyWayPlayerController::StartInteract(FKey Key)
{
	GameInstance->GlobalEventHandler->OnInputActionBindingEvent.Broadcast(InputEvents::StartInteract, Key);
}

void ATheWhiskyWayPlayerController::EndInteract(FKey Key)
{
	GameInstance->GlobalEventHandler->OnInputActionBindingEvent.Broadcast(InputEvents::EndInteract, Key);
}

void ATheWhiskyWayPlayerController::LMBPressed(FKey Key)
{
	GameInstance->GlobalEventHandler->OnInputActionBindingEvent.Broadcast(InputEvents::RepairStart, Key);
}

void ATheWhiskyWayPlayerController::LMBReleased(FKey Key)
{
	GameInstance->GlobalEventHandler->OnInputActionBindingEvent.Broadcast(InputEvents::RepairEnd, Key);
}

void ATheWhiskyWayPlayerController::RMBPressed(FKey Key)
{
	GameInstance->GlobalEventHandler->OnInputActionBindingEvent.Broadcast(InputEvents::Throw, Key);
}

void ATheWhiskyWayPlayerController::EnterPressed(FKey Key)
{
	GameInstance->GlobalEventHandler->OnInputActionBindingEvent.Broadcast(InputEvents::Hack, Key);
}

void ATheWhiskyWayPlayerController::SpacePressed(FKey Key)
{
	GameInstance->GlobalEventHandler->OnInputActionBindingEvent.Broadcast(InputEvents::QTE, Key);
}


void ATheWhiskyWayPlayerController::SpaceReleased(FKey Key)
{
	GameInstance->GlobalEventHandler->OnInputActionBindingEvent.Broadcast(InputEvents::QTE, Key);
	GameInstance->GlobalEventHandler->OnInputActionBindingReleaseEvent.Broadcast(InputEvents::QTE, Key);
}

void ATheWhiskyWayPlayerController::AnyKey(FKey Key)
{
	GameInstance->GlobalEventHandler->OnInputActionBindingEvent.Broadcast(InputEvents::AnyKey, Key);
}

void ATheWhiskyWayPlayerController::PausePressed()
{
    GameInstance->GlobalEventHandler->OnPauseEvent.Broadcast();
}

void ATheWhiskyWayPlayerController::WalkPressed(FKey Key)
{
	GameInstance->GlobalEventHandler->OnInputActionBindingEvent.Broadcast(InputEvents::WalkStart, Key);
}

void ATheWhiskyWayPlayerController::WalkReleased(FKey Key)
{
	GameInstance->GlobalEventHandler->OnInputActionBindingEvent.Broadcast(InputEvents::WalkStop, Key);
}


