// Fill out your copyright notice in the Description page of Project Settings.

#include "TheWhiskyWay.h"
#include "TheWhiskyWayCharacter.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

// Sets default values
ATheWhiskyWayCharacter::ATheWhiskyWayCharacter()
{
    // Set size for collision capsule
    GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

    //Create Camera Component
    FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
    FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
    FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
    FirstPersonCameraComponent->bUsePawnControlRotation = true;

    // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ATheWhiskyWayCharacter::BeginPlay()
{
    Super::BeginPlay();

	// Setup default values
	bIsNearMachine = false;
	bIsPlayerInteracting = false;
	bMayLookAndMove = false;
	bIsInputAllowed = true;

	CanAmount = 1.f;
	CameraFadeDuration = 1.f;

	PlayerController = Cast<APlayerController>(GetController());
	GameInstance = Cast<UTheWhiskyWayGameInstance>(GetGameInstance());
    
	GameInstance->GlobalEventHandler->OnInputAxisMappingEvent.AddDynamic(this, &ATheWhiskyWayCharacter::OnInputAxisMappingEventHandler);
	GameInstance->GlobalEventHandler->OnInputActionBindingEvent.AddDynamic(this, &ATheWhiskyWayCharacter::OnInputActionBindingEventHandler);
	GameInstance->GlobalEventHandler->OnInteractableFixedEvent.AddDynamic(this, &ATheWhiskyWayCharacter::OnInteractableFixedEventHandler);
	GameInstance->GlobalEventHandler->OnViewChangedEvent.AddDynamic(this, &ATheWhiskyWayCharacter::OnViewChangedEventHandler);
	GameInstance->GlobalEventHandler->OnHackingActiveEvent.AddDynamic(this, &ATheWhiskyWayCharacter::OnHackingActiveEventHandler);
	GameInstance->GlobalEventHandler->OnOutroStartedEvent.AddDynamic(this, &ATheWhiskyWayCharacter::OnOutroStartedEventHandler);
}

// Called every frame
void ATheWhiskyWayCharacter::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );
    
    if (bIsNearMachine)
    {
		Raycast();
        Machine->AlcoholLevel = AlcoholLevel;
    }
	AlcoholFlow(DeltaTime);
}

void ATheWhiskyWayCharacter::Raycast() //raycast for interactables
{
    //UE_LOG(LogTemp, Warning, TEXT("Servus"));
    FHitResult* HitResult = new FHitResult();
    FVector StartTrace = FirstPersonCameraComponent->GetComponentLocation();
    FVector ForwardVector = FirstPersonCameraComponent->GetForwardVector();
    FVector EndTrace = (ForwardVector * 300) + StartTrace;
    FCollisionQueryParams* CQP = new FCollisionQueryParams();

    if (GetWorld()->LineTraceSingleByChannel(*HitResult, StartTrace, EndTrace, ECC_Visibility, *CQP))
    {
		if (Cast<ATheWhiskyWayInteractable>(HitResult->GetActor()) && bMayLookAndMove)
		{
			//do when interactable spotted by raycast
				bIsInteractableSpotted = true;
				InteractableObject = Cast<ATheWhiskyWayInteractable>(HitResult->GetActor());
				GameInstance->GlobalEventHandler->OnInteractableSpottedEvent.Broadcast(bIsInteractableSpotted, InteractableObject, AlcoholLevel);
		}
		else if (bIsInteractableSpotted)
		{
			bIsInteractableSpotted = false;
			GameInstance->GlobalEventHandler->OnInteractableSpottedEvent.Broadcast(bIsInteractableSpotted, this, AlcoholLevel);
		}
    }
}

void ATheWhiskyWayCharacter::OnInputAxisMappingEventHandler(InputEvents InputEvent, float InputVal)
{
	if (bIsInputAllowed)
	{
		if (InputEvent == InputEvents::Turn) {
			Turn(InputVal);
		}
		if (InputEvent == InputEvents::LookUp) {
			LookUp(InputVal);
		}
		if (InputEvent == InputEvents::Forward) {
			MoveForward(InputVal);
		}
		if (InputEvent == InputEvents::Right) {
			MoveRight(InputVal);
		}
	}
}

void ATheWhiskyWayCharacter::OnInputActionBindingEventHandler(InputEvents InputEvent, FKey Key)
{
	if (bIsInputAllowed)
	{
		if (InputEvent == InputEvents::Drink) {
			if (bMayLookAndMove && bMayDrink)
			{
				Drink();
			}
		}
		if (InputEvent == InputEvents::StartInteract) {
			if (!bIsPlayerHacking)
			{
				ToggleMachineMaintanance();
			}
		}
		if (InputEvent == InputEvents::EndInteract) {
			if (bMayLookAndMove)
			{
				ToggleMachineMaintanance();
			}
		}
		if (InputEvent == InputEvents::RepairStart) {
			GameInstance->GlobalEventHandler->OnPlayerRepairEvent.Broadcast(InteractableObject, true, AlcoholLevel);
		}
		if (InputEvent == InputEvents::RepairEnd) {
			GameInstance->GlobalEventHandler->OnPlayerRepairEvent.Broadcast(InteractableObject, false, AlcoholLevel);
		}
	}
}

void ATheWhiskyWayCharacter::OnInteractableFixedEventHandler(AActor* Actor)
{
	FocusOnMaintananceArea(false, this);
}

void ATheWhiskyWayCharacter::OnHackingActiveEventHandler(bool bIsHacking) 
{
	bIsPlayerHacking = bIsHacking;
}

void ATheWhiskyWayCharacter::Drink()
{
	float CalcSteps = 3.f;
	float maxAlcInput = 1.f / CalcSteps;

	if (bIsFirstDrink)
	{
		AlcoholLevel = 1.f;
		bIsFirstDrink = false;
	}
	else if (AlcoholLevel + maxAlcInput > 1.f)
	{
		CanAmount = CanAmount - (AlcoholLevel + maxAlcInput - 1.f);
		AlcoholLevel = 1.f;
	}
	else
	{
		AlcoholLevel = AlcoholLevel + maxAlcInput;
		CanAmount = CanAmount - maxAlcInput;
	}
}

void ATheWhiskyWayCharacter::OnViewChangedEventHandler(bool bIsPlayerView)
{
	bMayLookAndMove = bIsPlayerView;
}

void ATheWhiskyWayCharacter::OnOutroStartedEventHandler() 
{
	bIsInputAllowed = false;
}

void ATheWhiskyWayCharacter::AlcoholFlow(float DeltaTime)
{
	if (AlcoholLevel > 0.f && bMayLookAndMove)
	{
		AlcoholLevel = AlcoholLevel - (DeltaTime * (1.f / 60.f));
	}
	GameInstance->GlobalEventHandler->OnAlcoholLevelChangedEvent.Broadcast(AlcoholLevel);
}


void ATheWhiskyWayCharacter::ToggleMachineMaintanance()
{
    if (MayStartMachineMaintanance())
    {
        FocusOnMaintananceArea(true, InteractableObject);
    }
    else if (MayEndMachineMaintanance())
    {
        FocusOnMaintananceArea(false, this);
    }
}



bool ATheWhiskyWayCharacter::MayStartMachineMaintanance()
{
    if (!bIsPlayerInteracting && bIsInteractableSpotted && !InteractableObject->bIsInteractableFixed  && (!InteractableObject->bIsCritical || (InteractableObject->bIsCritical && AlcoholLevel <= 0.f)))
    {
		return true;
    }
    else 
    {
        return false;
    }
}



bool ATheWhiskyWayCharacter::MayEndMachineMaintanance()
{
    if (bIsPlayerInteracting)
    {
        return true;
    }
    else
    {
        return false;
    }
}



void ATheWhiskyWayCharacter::FocusOnMaintananceArea(bool bMayFocus, AActor* Actor)
{
    bIsPlayerInteracting = bMayFocus;
	GameInstance->GlobalEventHandler->OnIsPlayerInteractingEvent.Broadcast(bMayFocus, Actor, AlcoholLevel);
}


void ATheWhiskyWayCharacter::Turn(float RotVal)
{
    if (bMayLookAndMove)
    {
        AddControllerYawInput(RotVal);
    } 
}

void ATheWhiskyWayCharacter::LookUp(float RotVal)
{
    if (bMayLookAndMove)
    {
        AddControllerPitchInput(RotVal);
    }
}

void ATheWhiskyWayCharacter::MoveForward(float MoveValue)
{
    if (bMayLookAndMove)
    {
        if (Controller != NULL && MoveValue != 0.0f)
        {
            FRotator FPSTutorialCharacterRotation = Controller->GetControlRotation();

            if (GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling())
            {
                FPSTutorialCharacterRotation.Pitch = 0.0f;
            }

            const FVector MoveDirection = FRotationMatrix(FPSTutorialCharacterRotation).GetScaledAxis(EAxis::X);
            AddMovementInput(MoveDirection, MoveValue);
        }
    }
}

void ATheWhiskyWayCharacter::MoveRight(float MoveValue)
{
    if (bMayLookAndMove)
    {
        if (Controller != NULL && MoveValue != 0.0f)
        {
            FRotator FPSTutorialCharacterRotation = Controller->GetControlRotation();

            const FVector MoveDirection = FRotationMatrix(FPSTutorialCharacterRotation).GetScaledAxis(EAxis::Y);

            AddMovementInput(MoveDirection, MoveValue);
        }
    }
}
