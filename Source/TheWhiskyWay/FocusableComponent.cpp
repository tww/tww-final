// Fill out your copyright notice in the Description page of Project Settings.

#include "TheWhiskyWay.h"
#include "FocusableComponent.h"


// Sets default values for this component's properties
UFocusableComponent::UFocusableComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UFocusableComponent::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld()->GetGameInstance())
	{
		GameInstance = Cast<UTheWhiskyWayGameInstance>(GetWorld()->GetGameInstance());
		GameInstance->GlobalEventHandler->OnIsPlayerInteractingEvent.AddDynamic(this, &UFocusableComponent::OnInteractionInputEventHandler);
	}

	CameraFadeDuration = 1.f;
}


// Called every frame
void UFocusableComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UFocusableComponent::OnInteractionInputEventHandler(bool IsFocusingInteractable, AActor* Actor, float NewAlcoholLevel)
{
		if (Actor == GetOwner())
		{
			if ((bIsAffectedByAlcohol && !IsDrunk(NewAlcoholLevel)) || !bIsAffectedByAlcohol)
			{
				APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
				PC->SetViewTargetWithBlend(Actor, CameraFadeDuration, VTBlend_Linear, 0., true);
				OwningInteractable->bIsFocused = true;
				GameInstance->GlobalEventHandler->OnStartInteractionViewEvent.Broadcast();
				GameInstance->GlobalEventHandler->OnViewChangedEvent.Broadcast(false);
			}
		}
		else if (OwningInteractable->bIsFocused && !OwningInteractable->bIsHackingInputEnabled)
		{
			APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			PC->SetViewTargetWithBlend(Actor, CameraFadeDuration, VTBlend_Linear, 0., true);
			OwningInteractable->bIsFocused = false;
			GameInstance->GlobalEventHandler->OnLeaveInteractionViewEvent.Broadcast();
			//Wait blending time
			GetWorld()->GetTimerManager().SetTimer(CameraBlendTimerHandle, this, &UFocusableComponent::CallEventBlendAfterDelay, CameraFadeDuration, false);
		}
}

bool UFocusableComponent::IsDrunk(float AlcoholLevel)
{
	if (AlcoholLevel > 0.f)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void UFocusableComponent::CallEventBlendAfterDelay()
{
	GameInstance->GlobalEventHandler->OnViewChangedEvent.Broadcast(true);
}

