// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TheWhiskyWayEnums.h"
#include "CoreMinimal.h"
#include "InteractableComponent.h"
#include "GlowableComponent.generated.h"


UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THEWHISKYWAY_API UGlowableComponent : public UInteractableComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGlowableComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	bool bIsAffectedByAlcohol;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	MachineName DependingMachine;
	
};
