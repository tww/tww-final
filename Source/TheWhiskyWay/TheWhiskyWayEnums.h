#pragma once

// Game Stuff

UENUM(BlueprintType)
enum class GameState : uint8
{
    NotInitialized  UMETA(DisplayName = "Not Initialized"),
    VisciousCircle  UMETA(DisplayName = "Viscious Circle"),
    GameOver        UMETA(DisplayName = "Game Over")
};

UENUM(BlueprintType)
enum class CircleState : uint8
{
    Mixed           UMETA(DisplayName = "Mixed"),
    AllQuickFixed   UMETA(DisplayName = "All Quick-Fixed"),
    AllFixed        UMETA(DisplayName = "All Fixed")
};

// Machine Stuff

UENUM(BlueprintType)
enum class MachineState : uint8
{
    NotFixable  UMETA(DisplayName = "Not Fixable"),
    Broken      UMETA(DisplayName = "Broken"),
    QuickFixed  UMETA(DisplayName = "Quick-Fixed"),
    Fixed       UMETA(DisplayName = "Fixed")
};

UENUM(BlueprintType)
enum class MachineName : uint8
{
	SixBack				UMETA(DisplayName = "SixPack"),
    GravityGenerator    UMETA(DisplayName = "Gravity Generator"),
    PhotonEngager       UMETA(DisplayName = "Photon Engager"),
    HeatDisperser       UMETA(DisplayName = "Heat Disperser"),
	Door				UMETA(DisplayName = "Final Door"),
};

UENUM(BlueprintType)
enum class Interactable : uint8
{
    Screw       UMETA(DisplayName = "Screw"),
    Nut         UMETA(DisplayName = "Nut"),
    Computer    UMETA(DisplayName = "Computer"),
    Harddrive   UMETA(DisplayName = "Harddrive"),
    Valve       UMETA(DisplayName = "Valve"),
    Button      UMETA(DisplayName = "Button"),
	DoorButton      UMETA(DisplayName = "Final Button")
};

// Character Stuff

UENUM(BlueprintType)
enum class Character : uint8
{
    Djona   UMETA(DisplayName = "Djona"),
    MAAI    UMETA(DisplayName = "MAAI")
};

// Sound Stuff

UENUM(BlueprintType)
enum class EventType : uint8
{
    Marker      UMETA(DisplayName = "Marker"),
    EndOfEvent  UMETA(DisplayName = "End of Event"),
    Undefined   UMETA(DisplayName = "Undefined")
};

UENUM(BlueprintType)
enum class InputEvents : uint8
{
	LookUp	  UMETA(DisplayName = "Look Up Event"),
	Turn	  UMETA(DisplayName = "Turn Event"),
	Forward   UMETA(DisplayName = "Move Forward Event"),
	Right     UMETA(DisplayName = "Move Right Event"),
	Drink	  UMETA(DisplayName = "Drink Event"),
	Sprint	  UMETA(DisplayName = "Sprint Event"),
	StartInteract  UMETA(DisplayName = "E Interaction Start Event"),
	EndInteract  UMETA(DisplayName = "E Interaction End Event"),
	Throw	UMETA(DisplayName = "Throw Can Event"),
	RepairStart	  UMETA(DisplayName = "Mouse Pressed Event"),
	RepairEnd	  UMETA(DisplayName = "Mouse Released Event"),
	Hack	  UMETA(DisplayName = "Enter Interaction Event"),
	QTE		  UMETA(DisplayName = "Space Quick Time Event"),
	AnyKey    UMETA(DisplayName = "All Keys"),
    Pause UMETA(DisplayName = "Pause"),
	WalkStart UMETA(DisplayName = "Walk Start"),
	WalkStop UMETA(DisplayName = "Walk Stop")
};

UENUM(BlueprintType)
enum class PromptTypes : uint8
{
	Move	  UMETA(DisplayName = "Move Prompt"),
	StartInteraction	  UMETA(DisplayName = "Start Interaction Prompt"),
	EndInteraction	  UMETA(DisplayName = "End Interaction Prompt"),
	Docking		UMETA(DisplayName = "Docking Prompt"),
	UseTool		UMETA(DisplayName = "Use Tool Prompt"),
	Screwing	  UMETA(DisplayName = "Screwing Prompt"),
	Pulling	  UMETA(DisplayName = "Pulling Prompt"),
	Hacking	  UMETA(DisplayName = "Hacking Prompt"),
	DragAndDrop UMETA(DisplayName = "Drag&Drop Prompt"),
};

UENUM(BlueprintType)
enum class ELevelName : uint8
{
    MainMenu        UMETA(DisplayName = "Main Menu"),
    Intro           UMETA(DisplayName = "Intro"),
    Level           UMETA(DisplayName = "Level"),
    Credits         UMETA(DisplayName = "Credits"),
};
