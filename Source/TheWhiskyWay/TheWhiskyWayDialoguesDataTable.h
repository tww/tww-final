// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataTable.h"
#include "TheWhiskyWayEnums.h"
#include "TheWhiskyWayDialoguesDataTable.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct THEWHISKYWAY_API FTheWhiskyWayDialoguesDataTable : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
	
public:

    FTheWhiskyWayDialoguesDataTable() 
        : character(Character::MAAI)
        , line("")
    {}

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Dialogues)
    Character character;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Dialogues)
    FString line;

};
