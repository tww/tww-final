// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TheWhiskyWayEnums.h"
#include "TheWhiskyWayStructs.h"
#include "Components/ActorComponent.h"
#include "AkAudioEvent.h"
#include "TheWhiskyWayGlobalEventHandler.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPlayerInputAxisMappingEventDelegate, InputEvents, InputEvent, float, InputVal);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPlayerInputActionBindingEventDelegate, InputEvents, InputEvent, FKey, InputKey);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPlayerInputActionBindingReleaseEventDelegate, InputEvents, InputEvent, FKey, InputKey);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FPlayerRepairEvent, AActor*, Actor, bool, startsRepairing, float, AlcoholLevel);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPlayerAlcoholLevelChangedEventDelegate, float, NewAlcoholLevel);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FPlayerNearMachineEventDelegate, bool, IsNearMachine, AActor*, Actor, float, AlcoholLevel);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FPlayerSpottedInteractableEventDelegate, bool, bIsSpotted, AActor*, Actor, float, NewAlcoholLevel);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FComponentOutlinedEventDelegate, bool, isOutlined, AActor*, Actor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FPlayerFocusesInteractableEventDelegate, bool, IsFocusingInteractable, AActor*, Actor, float, NewAlcoholLevel);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FBreakInteractableEventDelegate, AActor*, Actor, float, BreakDelay);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHackingActiveEventDelegate, bool, bIsHacking);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FInteractableBrokenEventDelegate, AActor*, Actor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FInteractableFixedEventDelegate, AActor*, Actor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FBreakMachineEventDelegate, MachineName, Machine, float, BreakDelay);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMultitoolSpawnedEventDelegate, AActor*, Actor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMultitoolDockedEventDelegate, AActor*, Actor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FButtonPushedEventDelegate, AActor*, Actor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FResetButtonsEventDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FViewChangedEventDelegate, bool, bIsPlayerView);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FStartInteractionViewEventDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FLeaveInteractionViewEventDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FShowPromptEventDelegate, PromptTypes, PromptType, float, PosX, float, PosY);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDrinkingActivatedEventDelegate);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FChangeMachineStateEventDelegate, MachineName, MachineName, MachineState, newMachineState, bool, PlayDialogue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FChangeMachineStateCompletedEventDelegate);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDialogueEventDelegate, UAkAudioEvent*, DialogueEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FSoundEventDelegate, UAkAudioEvent*, SoundEvent, AActor*, Actor);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnOpenDoors);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameOverEventDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWinEventDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnFadeOutEventDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnOutroStartedEventDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRestartEvent);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnLevelChangeEventDelegate, ELevelName, newLevel, bool, bShowLoadingScreen, float, WaitDuration);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FLevelChangeCompleteDelegate, ELevelName, newLevel);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPauseEventDelegate);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWwiseCallbackEventDelegate, FSoundCallbackInfo, CallbackInfo);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THEWHISKYWAY_API UTheWhiskyWayGlobalEventHandler : public UActorComponent
{
    GENERATED_BODY()

public: 
    // Sets default values for this component's properties
    UTheWhiskyWayGlobalEventHandler();

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Input Events")
	FPlayerInputAxisMappingEventDelegate OnInputAxisMappingEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Input Events")
	FPlayerInputActionBindingEventDelegate OnInputActionBindingEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Input Events")
	FPlayerInputActionBindingReleaseEventDelegate OnInputActionBindingReleaseEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Input Events")
	FPlayerRepairEvent OnPlayerRepairEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FPlayerAlcoholLevelChangedEventDelegate OnAlcoholLevelChangedEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FPlayerNearMachineEventDelegate OnNearMachineEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FPlayerSpottedInteractableEventDelegate OnInteractableSpottedEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FComponentOutlinedEventDelegate OnComponentOutlinedEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FPlayerFocusesInteractableEventDelegate OnIsPlayerInteractingEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FBreakInteractableEventDelegate OnBreakInteractableEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FHackingActiveEventDelegate OnHackingActiveEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FInteractableBrokenEventDelegate OnInteractableBrokenEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FInteractableFixedEventDelegate OnInteractableFixedEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FBreakMachineEventDelegate OnBreakMachineEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FMultitoolSpawnedEventDelegate OnMultitoolSpawnedEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FMultitoolDockedEventDelegate OnMultitoolDockedEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FViewChangedEventDelegate OnViewChangedEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FStartInteractionViewEventDelegate OnStartInteractionViewEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FLeaveInteractionViewEventDelegate OnLeaveInteractionViewEvent;

    UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
    FButtonPushedEventDelegate OnButtonPushedEvent;

    UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
    FResetButtonsEventDelegate OnResetButtonsEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
	FShowPromptEventDelegate OnShowPromptEvent;

    UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
    FChangeMachineStateEventDelegate OnChangeMachineStateEvent;
    
    UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game Events")
    FChangeMachineStateCompletedEventDelegate OnChangeMachineStateCompletedEvent;

    UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Audio")
    FOnWwiseCallbackEventDelegate OnWwiseCallbackEvent;
    
    UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Audio")
    FDialogueEventDelegate OnDialogueEvent;

    UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Audio")
    FSoundEventDelegate OnSoundEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Audio")
	FOnOpenDoors OnOpenDoorsEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game State Events")
	FOnGameOverEventDelegate OnGameOverEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game State Events")
	FOnWinEventDelegate OnWinEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game State Events")
	FOnOutroStartedEventDelegate OnOutroStartedEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game State Events")
	FOnFadeOutEventDelegate OnFadeOutEvent;

    UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game State Events")
    FOnPauseEventDelegate OnPauseEvent;

    UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game State Events")
    FOnLevelChangeEventDelegate OnLevelChangeEvent;

    UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game State Events")
    FLevelChangeCompleteDelegate OnLevelChangeCompleteEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game State Events")
	FOnDrinkingActivatedEventDelegate OnDrinkingActivatedEvent;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "The Whisky Way|Game State Events")
	FOnRestartEvent OnRestartEvent;

protected:
    // Called when the game starts
    virtual void BeginPlay() override;

public: 
    // Called every frame
    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
