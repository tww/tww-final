// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "TheWhiskyWayEnums.h"
#include "TheWhiskyWayAudioController.h"
#include "TheWhiskyWayGameMode.generated.h"

/**
 * 
 */
UCLASS()
class THEWHISKYWAY_API ATheWhiskyWayGameMode : public AGameMode
{
    GENERATED_BODY()

    ATheWhiskyWayGameMode(const FObjectInitializer& ObjectInitializer);

protected:
    virtual void StartPlay() override;
};
