// Fill out your copyright notice in the Description page of Project Settings.

#include "TheWhiskyWay.h"
#include "TheWhiskyWayInteractable.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

// Sets default values
ATheWhiskyWayInteractable::ATheWhiskyWayInteractable()
{
	
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InteractableRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Interactable Root"));
	RootComponent = InteractableRoot;
	
	//Create Camera Component
	InteractableCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("InteractableCamera"));
	InteractableCameraComponent->RelativeLocation = FVector(0.f, 0.f, 0.f); // Position of the camera
	InteractableCameraComponent->SetupAttachment(InteractableRoot);
}

// Called when the game starts or when spawned
void ATheWhiskyWayInteractable::BeginPlay()
{
	Super::BeginPlay();

	GameInstance = Cast<UTheWhiskyWayGameInstance>(GetGameInstance());

	GameInstance->GlobalEventHandler->OnIsPlayerInteractingEvent.AddDynamic(this, &ATheWhiskyWayInteractable::OnIsPlayerInteractingEventHandler);
	
	bIsPlayerRepairing = false;
	bIsPlayerInteracting = false;
	bIsInteractableFixed = true;

	OriginLocation = InteractableMesh->GetComponentLocation();
	OriginRotation = InteractableMesh->GetComponentRotation();
	
}

// Called every frame
void ATheWhiskyWayInteractable::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void ATheWhiskyWayInteractable::SetupInteractableSpecs(bool bIsCriticalInteractable, Interactable InteractableSpecification)
{
	bIsCritical = bIsCriticalInteractable;
	Specification = InteractableSpecification;
}

void ATheWhiskyWayInteractable::OnIsPlayerInteractingEventHandler(bool isInteractableFocused, AActor* Actor, float NewAlcoholLevel)
{
	if (Cast<ATheWhiskyWayInteractable>(Actor))
	{
		if (Cast<ATheWhiskyWayInteractable>(Actor)->Specification == this->Specification)
		{
			bIsPlayerInteracting = isInteractableFocused;
			AlcoholLevel = NewAlcoholLevel;
		}
	}
	else if (!isInteractableFocused)
	{
		bIsPlayerInteracting = isInteractableFocused;
	}
}
