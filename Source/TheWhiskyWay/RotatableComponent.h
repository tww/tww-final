// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TheWhiskyWayGameInstance.h"
#include "TheWhiskyWayInteractable.h"
#include "CoreMinimal.h"
#include "InteractableComponent.h"
#include "RotatableComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THEWHISKYWAY_API URotatableComponent : public UInteractableComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	URotatableComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UTheWhiskyWayGameInstance* GameInstance;

	APlayerController* PlayerController;

	UFUNCTION(BlueprintCallable, Category = "Player Input Events")
	void OnInputActionBindingEventHandler(AActor* Actor, bool isRepairing, float AlcoholLevel);

	UFUNCTION(BlueprintCallable, Category = "Player Input Events")
	void OnBreakInteractableEventHandler(AActor* Actor, float Delay);

	UFUNCTION(BlueprintCallable, Category = "Player Input Events")
	void OnMultitoolDockedEventHandler(AActor* Actor);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	float Movement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	float Rotation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	bool bIsAffectedByMultitool;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	bool bIsAffectedByAlcohol;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	bool bIsRotationOnly;

	float MaxMovement;
	float TrackingTime;
	float RotationSpeed;
	float BreakDelay;
	float MovementPerSecond;
	float StopCounter;
	bool bIsPlayerRepairing;
	bool bIsMultitoolDocked;
	bool bMayLooseInteractable;
	FVector2D MousePosition;
	TArray<FVector2D> SaveMousePosition;
	FTimerHandle PositionLoggerTimerHandle;

	void LooseInteractable(float DeltaTime);
	void TrackMousePosition();
	void CheckMouseRotation();
	void RotateAndMove(float DeltaTime);
	void Rotate(float DeltaTime);
	void ResetComponent();
	bool IsDrunk(float AlcoholLevel);
	
};
