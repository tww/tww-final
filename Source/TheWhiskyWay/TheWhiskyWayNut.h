// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TheWhiskyWayInteractable.h"
#include "TheWhiskyWayNut.generated.h"

/**
 * 
 */
UCLASS()
class THEWHISKYWAY_API ATheWhiskyWayNut : public ATheWhiskyWayInteractable
{
	GENERATED_BODY()

public:
	ATheWhiskyWayNut();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
};
