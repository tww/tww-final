// Fill out your copyright notice in the Description page of Project Settings.

#include "TheWhiskyWay.h"
#include "OutlinableComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

// Sets default values for this component's properties
UOutlinableComponent::UOutlinableComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOutlinableComponent::BeginPlay()
{
	Super::BeginPlay();

	bIsSpotted = false;

	GlobalEventHandler->OnInteractableSpottedEvent.AddDynamic(this, &UOutlinableComponent::OnSpottedEventHandler);
	GlobalEventHandler->OnNearMachineEvent.AddDynamic(this, &UOutlinableComponent::OnPlayerNearMachineEventHandler);
}


// Called every frame
void UOutlinableComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UOutlinableComponent::OnSpottedEventHandler(bool bIsInteractableSpotted, AActor * Actor, float NewAlcoholLevel)
{	
	if (Actor->GetName().Equals(OwningInteractable->GetName()) && !OwningInteractable->bIsInteractableFixed && ((bIsAffectedByAlcohol && !IsDrunk(NewAlcoholLevel)) || !bIsAffectedByAlcohol))
	{
		if (bIsInteractableSpotted && !bIsSpotted)
		{
			bIsSpotted = true;
			OwningInteractable->InteractableMesh->SetRenderCustomDepth(bIsSpotted);
			GlobalEventHandler->OnComponentOutlinedEvent.Broadcast(bIsSpotted, OwningInteractable);
			GlobalEventHandler->OnShowPromptEvent.Broadcast(PromptTypes::StartInteraction, 0.f, 0.f);
		}
	}
	else
	{
		bIsSpotted = false;
		OwningInteractable->InteractableMesh->SetRenderCustomDepth(bIsSpotted);
		GlobalEventHandler->OnComponentOutlinedEvent.Broadcast(bIsSpotted, OwningInteractable);
	}
}

void UOutlinableComponent::OnPlayerNearMachineEventHandler(bool IsNearMachine, AActor* Actor, float AlcoholLevel)
{
	if (!IsNearMachine)
	{
		bIsSpotted = false;
		OwningInteractable->InteractableMesh->SetRenderCustomDepth(bIsSpotted);
		GlobalEventHandler->OnComponentOutlinedEvent.Broadcast(bIsSpotted, OwningInteractable);
	}
}

bool UOutlinableComponent::IsDrunk(float AlcoholLevel)
{
	if (AlcoholLevel > 0.f)
	{
		return true;
	}
	else
	{
		return false;
	}
}