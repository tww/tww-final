#include "TheWhiskyWay.h"
#include "TheWhiskyWayGameInstance.h"
#include "TheWhiskyWayAudioController.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

UTheWhiskyWayGameInstance::UTheWhiskyWayGameInstance(const FObjectInitializer & ObjectInitializer)
    : Super(ObjectInitializer)
{
}

void UTheWhiskyWayGameInstance::PostInitProperties()
{
    Super::PostInitProperties();
    //audioController = NewObject<ATheWhiskyWayAudioController>();
    GlobalEventHandler = NewObject<UTheWhiskyWayGlobalEventHandler>();
	GlobalEventHandler->OnRestartEvent.AddDynamic(this, &UTheWhiskyWayGameInstance::Setup);

    //audioController->GameInstance = this;
}

void UTheWhiskyWayGameInstance::Setup() 
{
	bMayDrink = false;
	bIsHealthbarActive = false;
	bIsFirstDrink = true;
	AlcoholLevel = 0.f;
}
