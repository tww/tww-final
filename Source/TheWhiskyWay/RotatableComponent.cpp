// Fill out your copyright notice in the Description page of Project Settings.

#include "TheWhiskyWay.h"
#include "RotatableComponent.h"


// Sets default values for this component's properties
URotatableComponent::URotatableComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void URotatableComponent::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld()->GetGameInstance())
	{
		GameInstance = Cast<UTheWhiskyWayGameInstance>(GetWorld()->GetGameInstance());
		GameInstance->GlobalEventHandler->OnPlayerRepairEvent.AddDynamic(this, &URotatableComponent::OnInputActionBindingEventHandler);
		GameInstance->GlobalEventHandler->OnBreakInteractableEvent.AddDynamic(this, &URotatableComponent::OnBreakInteractableEventHandler);
		GameInstance->GlobalEventHandler->OnMultitoolDockedEvent.AddDynamic(this, &URotatableComponent::OnMultitoolDockedEventHandler);
	}
	TrackingTime = 0.2f; //measured 0.2 is perfect time for rotation measuring
	StopCounter = 0.f;

	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
}
	


// Called every frame
void URotatableComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bMayLooseInteractable)
	{
		LooseInteractable(DeltaTime);
	}
	if (bIsPlayerRepairing && OwningInteractable->bIsFocused)
	{
		TrackMousePosition();
		if (bIsRotationOnly)
		{
			if (!OwningInteractable->bIsInteractableFixed)
			{
				Rotate(DeltaTime);
			}
		}
		else
		{
			if (!OwningInteractable->bIsInteractableFixed)
			{
				RotateAndMove(DeltaTime);
			}
		}
	}
}

void URotatableComponent::OnInputActionBindingEventHandler(AActor* Actor, bool isRepairing, float AlcoholLevel)
{
	
	if (Actor == GetOwner() && isRepairing)
	{	
		if ((bIsAffectedByAlcohol && !IsDrunk(AlcoholLevel)) || !bIsAffectedByAlcohol)
		{
			if ((bIsAffectedByMultitool && bIsMultitoolDocked) || !bIsAffectedByMultitool)
			{
				bIsPlayerRepairing = isRepairing;
				CheckMouseRotation();
			}
		}
		
	}
	else if (Actor == GetOwner() && !isRepairing) {
		bIsPlayerRepairing = isRepairing;
	}
}

void URotatableComponent::OnBreakInteractableEventHandler(AActor* Actor, float Delay)
{
	if (Actor == GetOwner())
	{
		bMayLooseInteractable = true;
		BreakDelay = Delay;
		MaxMovement = Cast<ATheWhiskyWayInteractable>(GetOwner())->OriginLocation.Z + Movement;
		MovementPerSecond = Movement / BreakDelay;
	}
}

void URotatableComponent::OnMultitoolDockedEventHandler(AActor* Actor) 
{
	if (Actor == GetOwner())
	{
		bIsMultitoolDocked = true;
	}
}

void URotatableComponent::LooseInteractable(float DeltaTime)
{
	if (Cast<ATheWhiskyWayInteractable>(GetOwner())->InteractableMesh->GetComponentLocation().Z < MaxMovement && Cast<ATheWhiskyWayInteractable>(GetOwner())->bIsInteractableFixed)
	{
		Cast<ATheWhiskyWayInteractable>(GetOwner())->InteractableMesh->AddLocalRotation(FRotator(.0f, -1.0f, .0f));
		Cast<ATheWhiskyWayInteractable>(GetOwner())->InteractableMesh->AddWorldOffset(Cast<ATheWhiskyWayInteractable>(GetOwner())->InteractableMesh->GetUpVector() * (DeltaTime * MovementPerSecond));
	}
	else
	{
		bMayLooseInteractable = false;
		Cast<ATheWhiskyWayInteractable>(GetOwner())->bIsInteractableFixed = false;
		GameInstance->GlobalEventHandler->OnInteractableBrokenEvent.Broadcast(GetOwner());
	}
}

void URotatableComponent::TrackMousePosition()
{

	UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetMousePosition(MousePosition.X, MousePosition.Y);
	//UE_LOG(LogTemp, Warning, TEXT("X: %f, Y: %f"), MousePosition.X, MousePosition.Y);
	SaveMousePosition.Add(MousePosition);
}

void URotatableComponent::CheckMouseRotation()
{
	if (SaveMousePosition.Num() > 2)
	{
		FVector2D FirstVector = SaveMousePosition[0];
		FVector2D SecondVector = SaveMousePosition[int(SaveMousePosition.Num() / 2)];
		FVector2D ThirdVector = SaveMousePosition[SaveMousePosition.Num() - 1];

		SaveMousePosition.Empty();

		FVector2D FirstSecondCathetus = FirstVector - SecondVector;
		FVector2D SecondThirdCathetus = ThirdVector - SecondVector;
		FVector2D FirstThirdHypotenuse = ThirdVector - FirstVector;

		FirstSecondCathetus.Normalize();
		SecondThirdCathetus.Normalize();

		float crossProduct = FVector2D::CrossProduct(FirstSecondCathetus, SecondThirdCathetus);

		float Distance = FirstThirdHypotenuse.Size();

		
		if (crossProduct < 0) //clockwise rotation
		{
			RotationSpeed = -(Distance * 0.001f);
		}
		else //no clockwise rotation
		{
			//RotationSpeed = (Distance * 0.001f);
		}
		
		//UE_LOG(LogTemp, Warning, TEXT("%f"), crossProduct);
	}
	
	if (bIsPlayerRepairing)
	{
		GetWorld()->GetTimerManager().SetTimer(PositionLoggerTimerHandle, this, &URotatableComponent::CheckMouseRotation, TrackingTime, false);
	}
	else
	{
		RotationSpeed = 0.f;
	}
}

void URotatableComponent::RotateAndMove(float DeltaTime)
{
	if (Cast<ATheWhiskyWayInteractable>(GetOwner())->InteractableMesh->GetComponentLocation().Z > Cast<ATheWhiskyWayInteractable>(GetOwner())->OriginLocation.Z && !Cast<ATheWhiskyWayInteractable>(GetOwner())->bIsInteractableFixed)
	{
		Cast<ATheWhiskyWayInteractable>(GetOwner())->InteractableMesh->AddLocalRotation(FRotator(.0f, (-RotationSpeed)*10.f, .0f));
		Cast<ATheWhiskyWayInteractable>(GetOwner())->InteractableMesh->AddWorldOffset((Cast<ATheWhiskyWayInteractable>(GetOwner())->InteractableMesh->GetUpVector() * RotationSpeed * DeltaTime));
	}
	else
	{
		Cast<ATheWhiskyWayInteractable>(GetOwner())->bIsInteractableFixed = true;
		ResetComponent();
		GameInstance->GlobalEventHandler->OnInteractableFixedEvent.Broadcast(GetOwner());
	}
}

void URotatableComponent::Rotate(float DeltaTime)
{

	if (StopCounter < 1.f )
	{
		Cast<ATheWhiskyWayInteractable>(GetOwner())->InteractableMesh->AddLocalRotation(FRotator(.0f, (-RotationSpeed)*1.5f, .0f));
		StopCounter += DeltaTime;
	}
	else
	{
		Cast<ATheWhiskyWayInteractable>(GetOwner())->bIsInteractableFixed = true;
		ResetComponent();
		GameInstance->GlobalEventHandler->OnInteractableFixedEvent.Broadcast(GetOwner());
	}
}

void URotatableComponent::ResetComponent()
{
	bIsPlayerRepairing = false;
	bIsMultitoolDocked = false;
	StopCounter = 0.f;
	RotationSpeed = 0.f;
	SaveMousePosition.Empty();
}

bool URotatableComponent::IsDrunk(float AlcoholLevel)
{
	if (AlcoholLevel > 0.f)
	{
		return true;
	}
	else
	{
		return false;
	}
}

