#pragma once

#include "TheWhiskyWay.h"
#include "TheWhiskyWayAudioController.h"
#include "TheWhiskyWayGameInstance.h"
#include "TheWhiskyWayGlobalEventHandler.h"
#include "TheWhiskyWayEnums.h"
#include "AkGameplayStatics.h"

bool ATheWhiskyWayAudioController::bCallbackMarker;
FSoundCallbackInfo ATheWhiskyWayAudioController::callbackInfo;
UTheWhiskyWayGameInstance* ATheWhiskyWayAudioController::GameInstance;

/**
* Callback function for markers and end of event
* @param in_eType Type of callback reason, in this case AK_Marker on reception of a marker event or AK_EndOfEvent on reception of a end of event event
* @param in_pCallbackInfo Pointer to callback information structure, in this case AkMarkerCallbackInfo* or AkEventCallbackInfo*
*/
void ATheWhiskyWayAudioController::Callback(AkCallbackType in_eType, AkCallbackInfo * in_pCallbackInfo)
{
    if (in_eType == AK_Marker)
    {
        AkMarkerCallbackInfo* markerInfo = (AkMarkerCallbackInfo*)in_pCallbackInfo;
        callbackInfo = FSoundCallbackInfo(EventType::Marker, markerInfo->strLabel);
        bCallbackMarker = true;
    }

    if (in_eType == AK_EndOfEvent)
    {
        AkEventCallbackInfo* eventInfo = (AkEventCallbackInfo*)in_pCallbackInfo;
        callbackInfo = FSoundCallbackInfo(EventType::EndOfEvent);
        bCallbackMarker = true;
    }
}

/**/

void ATheWhiskyWayAudioController::Tick(float deltaSeconds)
{
    Super::Tick(deltaSeconds);

    if (GetReceivedEvent())
    {
        OnEventReceivedEventHandler();
    }
}

bool ATheWhiskyWayAudioController::GetReceivedEvent()
{
    return bCallbackMarker;
}

void ATheWhiskyWayAudioController::SetReceivedEvent(bool receivedEvent)
{
    bCallbackMarker = receivedEvent;
}

FSoundCallbackInfo ATheWhiskyWayAudioController::GetCallbackInfo()
{
    return callbackInfo;
}

void ATheWhiskyWayAudioController::OnEventReceivedEventHandler()
{
    GameInstance->GlobalEventHandler->OnWwiseCallbackEvent.Broadcast(GetCallbackInfo());
    SetReceivedEvent(false);
}

/**/

ATheWhiskyWayAudioController::ATheWhiskyWayAudioController()
{
    PrimaryActorTick.bCanEverTick = true;
}

void ATheWhiskyWayAudioController::Init()
{
    LoadSoundBanks();

    GameInstance = Cast<UTheWhiskyWayGameInstance>(GetGameInstance());

    UWorld* World = GameInstance->GetWorld();

    if (World != nullptr && GameInstance != nullptr)
    {
        PlayerController = Cast<ATheWhiskyWayPlayerController>(UGameplayStatics::GetPlayerController(World, 0));
        GameInstance->GlobalEventHandler->OnDialogueEvent.AddDynamic(this, &ATheWhiskyWayAudioController::OnDialogueEventHandler);
        GameInstance->GlobalEventHandler->OnSoundEvent.AddDynamic(this, &ATheWhiskyWayAudioController::OnSoundEventHandler);
        GameInstance->GlobalEventHandler->OnWwiseCallbackEvent.AddDynamic(this, &ATheWhiskyWayAudioController::OnWwiseCallbackEventHandler);
    }
}

void ATheWhiskyWayAudioController::LoadSoundBanks()
{
    UAkGameplayStatics::LoadBankByName("Primary");
    UAkGameplayStatics::LoadBankByName("Music");
}

void ATheWhiskyWayAudioController::Queue(UAkAudioEvent* DialogueEventToQueue)
{
    DialogueEventQueue.Add(DialogueEventToQueue);
}

void ATheWhiskyWayAudioController::PlayNextDialogueEvent()
{
    if (DialogueEventQueue.Num() > 0)
    {
        UAkAudioEvent* DialogueEventToPlay = DialogueEventQueue[0];
        DialogueEventQueue.RemoveAt(0);
        OnDialogueEventHandler(DialogueEventToPlay);
    }
}

void ATheWhiskyWayAudioController::OnSoundEventHandler(UAkAudioEvent* SoundEvent, AActor* Actor)
{
    if (Actor != nullptr)
    {
        UAkGameplayStatics::PostEvent(SoundEvent, Actor);
    }
    else
    {
        UAkGameplayStatics::PostEvent(SoundEvent, PlayerController);
    }
}

void ATheWhiskyWayAudioController::OnDialogueEventHandler(UAkAudioEvent* DialogueEvent)
{
    const FString &EventName = GET_AK_EVENT_NAME(DialogueEvent, "");

    if (ShouldStopAllOthers(EventName))
    {
        StopAllOthersAndPlay(DialogueEvent);
    }
    else if (bAllowDialogue)
    {
        Play(DialogueEvent);
    }
    else if (ShouldBeQueued(EventName))
    {
        Queue(DialogueEvent);
    }
}

void ATheWhiskyWayAudioController::Play(UAkAudioEvent* DialogueEvent)
{
    bAllowDialogue = false;
    UAkGameplayStatics::PostEventWithCallback(DialogueEvent, PlayerController);
}

void ATheWhiskyWayAudioController::StopAllOthersAndPlay(UAkAudioEvent* DialogueEvent)
{
    if (!bAllowDialogue)
    {
        bStoppedDialogue = true;
        UAkGameplayStatics::PostEvent(nullptr, PlayerController, false, "StopAllDialogues");

    }
    Play(DialogueEvent);
}

bool ATheWhiskyWayAudioController::ShouldStopAllOthers(const FString &EventName)
{
    if (EventName.Equals("AllMachinesFixed")
        || EventName.Equals("GravityGeneratorDown")
        || EventName.Equals("HeatDisperserDown")
        || EventName.Equals("PhotonEngagerDown")
        || EventName.Equals("StuckInQuickFix")
        )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool ATheWhiskyWayAudioController::ShouldBeQueued(const FString &EventName)
{
    if (EventName.Equals("Intro_01")
        || EventName.Equals("Intro_02")
        || EventName.Equals("Intro_03")
        || EventName.Equals("LeavingDeck")
        || EventName.Equals("MachineQuickFixed")
        || EventName.Equals("RunningGag")
        || EventName.Equals("RunningGagResponseNormal")
        || EventName.Equals("RunningGagResponseAnnoyed")
        || EventName.Equals("RunningGagResponseRAGE")
        )
    {
        return true;
    }
    else
    {
        return false;
    }
}

void ATheWhiskyWayAudioController::OnAlcoholLevelChangedEventHandler(float newAlcoholLevel)
{
    UAkGameplayStatics::SetRTPCValue("Alcohol", newAlcoholLevel, 0, PlayerController);
}

void ATheWhiskyWayAudioController::OnWwiseCallbackEventHandler(FSoundCallbackInfo CallbackInfo)
{
    /*
    switch (CallbackInfo.type)
    {
    case EventType::Marker:
        CurrentSubtitle = FText::FromString(CallbackInfo.label);
        break;
    case EventType::EndOfEvent:
        if (bStoppedDialogue)
        {
            bStoppedDialogue = false;
        }
        else
        {
            CurrentSubtitle = FText::GetEmpty();
            bAllowDialogue = true;
            PlayNextDialogueEvent();
        }
        break;

    case EventType::Undefined:
        // fall through
    default:
        break;
    }
    */
}
