// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TheWhiskyWayGameInstance.h"
#include "TheWhiskyWayEnums.h"
#include "GameFramework/PlayerController.h"
#include "TheWhiskyWayPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class THEWHISKYWAY_API ATheWhiskyWayPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	ATheWhiskyWayPlayerController(const FObjectInitializer& ObjectInitializer);

	UTheWhiskyWayGameInstance* GameInstance;

	virtual void SetupInputComponent() override;

	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	FVector2D MousePosition;

	void OnOutroStartedEventHandler();

	void YawInput(float RotVal);
	void PitchInput(float RotVal);
		
	void MoveForward(float MoveVal);
	void MoveRight(float MoveVal);

	void Drink(FKey Key);

	void Sprint(FKey Key);

	void StartInteract(FKey Key);
	void EndInteract(FKey Key);

	void LMBPressed(FKey Key);
	void LMBReleased(FKey Key);

	void RMBPressed(FKey Key);

	void EnterPressed(FKey Key);

	void SpacePressed(FKey Key);
	void SpaceReleased(FKey Key);

	void AnyKey(FKey Key);

    void PausePressed();

	void WalkPressed(FKey Key);
	void WalkReleased(FKey Key);
};
