// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TheWhiskyWayGameInstance.h"
#include "TheWhiskyWayEnums.h"
#include "TheWhiskyWayInteractable.h"
#include "GameFramework/Actor.h"
#include "TheWhiskyWayMultitool.generated.h"

UCLASS()
class THEWHISKYWAY_API ATheWhiskyWayMultitool : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATheWhiskyWayMultitool();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
	bool bIsToolDocked;
};
