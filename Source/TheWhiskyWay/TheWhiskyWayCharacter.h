// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TheWhiskyWayGameInstance.h"
#include "TheWhiskyWayEnums.h"
#include "TheWhiskyWayMachine.h"
#include "TheWhiskyWayInteractable.h"
#include "GameFramework/Character.h"
#include "TheWhiskyWayCharacter.generated.h"

UCLASS()
class THEWHISKYWAY_API ATheWhiskyWayCharacter : public ACharacter
{
    GENERATED_BODY()

public:
    // Sets default values for this character's properties
    ATheWhiskyWayCharacter();

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    
    // Called every frame
    virtual void Tick(float DeltaSeconds) override;

    //First person camera
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
    UCameraComponent* FirstPersonCameraComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
	bool bIsInputAllowed;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interaction")
	bool bMayDrink;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interaction")
	bool bIsFirstDrink;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
    bool bIsNearMachine;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interaction")
    bool bIsPlayerInteracting;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interaction")
	bool bMayLookAndMove;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interaction")
    bool bIsInteractableSpotted;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interaction")
	bool bIsPlayerHacking;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interaction")
    float MouseTrackingTime;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interaction")
    float AlcoholLevel;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interaction")
	float CanAmount;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interaction")
    ATheWhiskyWayMachine* Machine;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interaction")
    ATheWhiskyWayInteractable* InteractableObject;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Events")
	UTheWhiskyWayGameInstance* GameInstance;
    
    UFUNCTION(BlueprintCallable, Category = "Player Input Events")
    void OnInputAxisMappingEventHandler(InputEvents InputEvent, float InputVal);

	UFUNCTION(BlueprintCallable, Category = "Player Input Events")
	void OnInputActionBindingEventHandler(InputEvents InputEvent, FKey Key);

	UFUNCTION(BlueprintCallable, Category = "Player Input Events")
	void OnInteractableFixedEventHandler(AActor* Actor);

	UFUNCTION(BlueprintCallable, Category = "Game Events")
	void OnViewChangedEventHandler(bool bIsPlayerView);

	UFUNCTION(BlueprintCallable, Category = "Game Events")
	void OnHackingActiveEventHandler(bool bIsHacking);

	UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Game State Events")
	void OnOutroStartedEventHandler();

protected:
    FVector2D MousePosition;
    TArray<FVector2D> SaveMousePosition;

    FTimerDelegate CameraBlendTimerDelegate;
    FTimerHandle CameraBlendTimerHandle;

    APlayerController* PlayerController;

    void ToggleMachineMaintanance();
    bool MayStartMachineMaintanance();
    bool MayEndMachineMaintanance();
    void FocusOnMaintananceArea(bool bMayFocus, AActor* Actor);
    void BlendPlayerCameraView(AActor* Actor);
    void Raycast();

	void Drink();
	void AlcoholFlow(float DeltaTime);

	float CameraFadeDuration;

    //handle looking
    UFUNCTION()
    void Turn(float RotVal);

    UFUNCTION()
    void LookUp(float RotVal);

    //handle movement
    UFUNCTION()
    void MoveForward(float MoveValue);

    UFUNCTION()
    void MoveRight(float MoveValue);
};
