// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TheWhiskyWayGameInstance.h"
#include "GameFramework/Actor.h"
#include "TheWhiskyWayEnums.h"
#include "TheWhiskyWayInteractable.generated.h"

UCLASS()
class THEWHISKYWAY_API ATheWhiskyWayInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATheWhiskyWayInteractable();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere)
	USceneComponent* InteractableRoot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
	UStaticMeshComponent* InteractableMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	UCameraComponent* InteractableCameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Events")
	UTheWhiskyWayGameInstance* GameInstance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
	Interactable Specification;

	UFUNCTION(BlueprintCallable, Category = "Player Input Events")
	void OnIsPlayerInteractingEventHandler(bool IsPlayerInteracting, AActor* Actor, float NewAlcoholLevel);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
	bool bIsCritical;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
	bool bIsPlayerInteracting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
	bool bIsPlayerRepairing;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
	bool bIsInteractableFixed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
	bool bIsToolDocked;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
	float AlcoholLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
	bool bIsHackingInputEnabled;

	bool bIsFocused;

	FVector OriginLocation;
	FRotator OriginRotation;

	void SetupInteractableSpecs(bool bIsCriticalInteractable, Interactable InteractableSpecification);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
	AActor* Multitool;
};

