// Fill out your copyright notice in the Description page of Project Settings.

#include "TheWhiskyWay.h"
#include "TheWhiskyWayNut.h"


ATheWhiskyWayNut::ATheWhiskyWayNut()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ATheWhiskyWayNut::BeginPlay()
{
	Super::BeginPlay();

	//UE_LOG(LogTemp, Warning, TEXT("huhu!"));
	ATheWhiskyWayInteractable::SetupInteractableSpecs(true, Interactable::Nut);
}

void ATheWhiskyWayNut::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
