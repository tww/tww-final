#pragma once

#include "TheWhiskyWayEnums.h"
#include "TheWhiskyWayStructs.generated.h"

USTRUCT(BlueprintType)
struct FSoundCallbackInfo
{
    GENERATED_BODY()

    FSoundCallbackInfo(EventType _type, FString _label)
    {
        type = _type;
        label = _label;
    }

    FSoundCallbackInfo(EventType _type)
    {
        type = _type;
        label = "";
    }

    FSoundCallbackInfo()
    {
        type = EventType::Undefined;
        label = "";
    }


public:
    UPROPERTY(BlueprintReadWrite)
        FString label;

    UPROPERTY(BlueprintReadWrite)
        EventType type;
};
