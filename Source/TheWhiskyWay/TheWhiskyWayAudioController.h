#pragma once

#include "TheWhiskyWayStructs.h"
#include "AkAudioDevice.h"
#include "AkAudioEvent.h"
#include "AkAudioClasses.h"
#include "TheWhiskyWayPlayerController.h"
#include "TheWhiskyWayAudioController.generated.h"

class UTheWhiskyWayGameInstance;

UCLASS()
class THEWHISKYWAY_API ATheWhiskyWayAudioController : public AActor
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "The Whisky Way|Audio")
    TArray<UAkAudioEvent*> DialogueEventQueue;
    
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "The Whisky Way|Audio")
	ATheWhiskyWayPlayerController* PlayerController;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "The Whisky Way|Audio")
    bool bAllowDialogue = true;
    
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "The Whisky Way|Audio")
	bool bStoppedDialogue = false;

    /**/

    static bool bCallbackMarker;
    static FSoundCallbackInfo callbackInfo;

    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "The Whisky Way|Audio")
    bool GetReceivedEvent();

    UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Audio")
    void SetReceivedEvent(bool receivedEvent);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category = "The Whisky Way|Audio")
    FSoundCallbackInfo GetCallbackInfo();

    UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Audio")
    void OnEventReceivedEventHandler();

    virtual void Tick(float deltaSeconds) override;

    /**/

    UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Audio")
    void LoadSoundBanks();

    static void Callback(AkCallbackType in_eType, AkCallbackInfo* in_pCallbackInfo);

    static UTheWhiskyWayGameInstance* GameInstance;

    ATheWhiskyWayAudioController();

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "The Whisky Way|Audio")
    FText CurrentSubtitle;

    UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Audio")
    void Queue(UAkAudioEvent* SoundEventToQueue);

    UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Audio")
    void PlayNextDialogueEvent();

    UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Audio")
    void Init();

    UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Audio")
    void OnSoundEventHandler(UAkAudioEvent* SoundEvent, AActor* Actor);

    UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Audio")
    void OnDialogueEventHandler(UAkAudioEvent* DialogueEvent);

    UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Audio")
    void OnAlcoholLevelChangedEventHandler(float newAlcoholLevel);

    UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Audio")
    void OnWwiseCallbackEventHandler(FSoundCallbackInfo CallbackInfo);

    UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Audio")
    void Play(UAkAudioEvent* DialogueEvent);

    UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Audio")
    void StopAllOthersAndPlay(UAkAudioEvent* DialogueEvent);

    UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Audio")
    bool ShouldStopAllOthers(const FString &EventName);

    UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Audio")
    bool ShouldBeQueued(const FString &EventName);
};
