// Fill out your copyright notice in the Description page of Project Settings.

#include "TheWhiskyWay.h"
#include "TheWhiskyWayAudioController.h"
#include "TheWhiskyWayMachine.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

int ATheWhiskyWayMachine::BrokenCount;
int ATheWhiskyWayMachine::QuickFixCount;

// Sets default values
ATheWhiskyWayMachine::ATheWhiskyWayMachine()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Setup Root Component 
	MachineRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Machine Root"));
	RootComponent = MachineRoot;

	// Setup Trigger Sphere
	MachineTriggerSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Machine Trigger Sphere"));
	MachineTriggerSphere->SetWorldScale3D(FVector(1.f, 1.f, 1.f));
	MachineTriggerSphere->bGenerateOverlapEvents = true;
	MachineTriggerSphere->SetupAttachment(MachineRoot);
}

// Called when the game starts or when spawned
void ATheWhiskyWayMachine::BeginPlay()
{
	Super::BeginPlay();
	SetupComponents();

	bIsFirstBreak = true;

	GameInstance = Cast<UTheWhiskyWayGameInstance>(GetGameInstance());
    GameInstance->GlobalEventHandler->OnChangeMachineStateEvent.AddDynamic(this, &ATheWhiskyWayMachine::OnChangeMachineStateEventHandler);
}

void ATheWhiskyWayMachine::EndPlay(EEndPlayReason::Type EndPlayReason)
{
    Super::EndPlay(EndPlayReason);

    BrokenCount = 0;
    QuickFixCount = 0;
}

// Called every frame
void ATheWhiskyWayMachine::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void ATheWhiskyWayMachine::SetupComponents()
{
	AlcoholLevel = .0f;

	MaxBreakDelay = 90.f;
	MinBreakDelay = 30.f;
}

void ATheWhiskyWayMachine::OnChangeMachineStateEventHandler(MachineName MachineName, MachineState newMachineState, bool PlayDialogue)
{
    if (CurrentMachineState == newMachineState)
    {
        return;
    }

    if (MachineName != CurrentMachineName)
    {
        return;
    }

    CurrentMachineState = newMachineState;

    switch (newMachineState)
    {
    case MachineState::NotFixable:
        // fall through
    case MachineState::Broken:
        if (PlayDialogue)
        {
            GameInstance->GlobalEventHandler->OnDialogueEvent.Broadcast(EventBroken);
			
            FString EventName = GET_AK_EVENT_NAME(EventBroken, "");
			FString gg = "GravityGeneratorDown";
			FString pe = "PhotonEngagerDown";
			FString hd = "HeatDisperserDown";

			if (EventName.Equals(gg))
			{
				GameInstance->audioController->CurrentSubtitle = FText::FromString("Attention! The Gravity Generator is operating at sub-optimal capacity.");

			}
			else if (EventName.Equals(pe))
			{
				GameInstance->audioController->CurrentSubtitle = FText::FromString("Attention! There is an abnormal pressure in the photon fluid pipelines.");
			}
			else if (EventName.Equals(hd))
			{
				GameInstance->audioController->CurrentSubtitle = FText::FromString("Attention! Error in heat dispersion detected.");
			}

            GameInstance->GlobalEventHandler->OnDialogueEvent.Broadcast(RunningGag);
            
            BrokenCount++;

            if (BrokenCount < 10)
            {
                GameInstance->GlobalEventHandler->OnDialogueEvent.Broadcast(RunningGagResponseNormal);
            }
            else if (BrokenCount < 20)
            {
                GameInstance->GlobalEventHandler->OnDialogueEvent.Broadcast(RunningGagResponseAnnoyed);
            }
            else
            {
                GameInstance->GlobalEventHandler->OnDialogueEvent.Broadcast(RunningGagResponseRAGE);
            }
        }
        break;
    case MachineState::QuickFixed:
        if (PlayDialogue)
        {
            QuickFixCount++;

            if (QuickFixCount == 12 || QuickFixCount == 15 || QuickFixCount >= 18)
            {
                GameInstance->GlobalEventHandler->OnDialogueEvent.Broadcast(StuckInQuickFix);
            }
            else
            {
                GameInstance->GlobalEventHandler->OnDialogueEvent.Broadcast(EventQuickFixed);
            }
        }
        break;
    case MachineState::Fixed:
        if (PlayDialogue)
        {
            GameInstance->GlobalEventHandler->OnDialogueEvent.Broadcast(EventFixed);
        }
        break;
    default:
        break;
    }

    GameInstance->GlobalEventHandler->OnChangeMachineStateCompletedEvent.Broadcast();
}

float ATheWhiskyWayMachine::CalculateDelayOnAlcoholLevel()
{
	return MaxBreakDelay - ((AlcoholLevel * 100)*((MaxBreakDelay - MinBreakDelay) / 100));
}

void ATheWhiskyWayMachine::BreakMachine()
{
	float Delay = CalculateDelayOnAlcoholLevel();
	GameInstance->GlobalEventHandler->OnBreakMachineEvent.Broadcast(CurrentMachineName, Delay);
	
}
