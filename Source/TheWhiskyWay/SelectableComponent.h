// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TheWhiskyWayGameInstance.h"
#include "TheWhiskyWayEnums.h"
#include "CoreMinimal.h"
#include "InteractableComponent.h"
#include "SelectableComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THEWHISKYWAY_API USelectableComponent : public UInteractableComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USelectableComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
