#pragma once

#include "Engine/GameInstance.h"
#include "TheWhiskyWayGlobalEventHandler.h"
#include "TheWhiskyWayGameInstance.generated.h"

class ATheWhiskyWayAudioController;

UCLASS()
class THEWHISKYWAY_API UTheWhiskyWayGameInstance : public UGameInstance
{
    GENERATED_BODY()
public:
    UTheWhiskyWayGameInstance(const FObjectInitializer & ObjectInitializer);

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    ATheWhiskyWayAudioController* audioController;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UTheWhiskyWayGlobalEventHandler* GlobalEventHandler;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bMayDrink;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsFirstDrink;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsHealthbarActive;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AlcoholLevel;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FName LevelToLoad;


    virtual void PostInitProperties() override;

	UFUNCTION(BlueprintCallable, Category = "Setup Game Data")
	void Setup();
};
