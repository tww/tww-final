// Fill out your copyright notice in the Description page of Project Settings.

#include "TheWhiskyWay.h"
#include "TheWhiskyWayGameMode.h"
#include "TheWhiskyWayHUD.h"
#include "TheWhiskyWayCharacter.h"
#include "Engine.h"

ATheWhiskyWayGameMode::ATheWhiskyWayGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	/*
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/02_Characters/01_Player/PTTheWhiskyWayCharacter_BP"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	HUDClass = ATheWhiskyWayHUD::StaticClass();

	//DefaultPawnClass = ATheWhiskyWayCharacter::StaticClass();
	*/
}

void ATheWhiskyWayGameMode::StartPlay()
{
	Super::StartPlay();
	StartMatch();
}
