// Fill out your copyright notice in the Description page of Project Settings.

#include "TheWhiskyWay.h"
#include "TheWhiskyWayScrew.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

ATheWhiskyWayScrew::ATheWhiskyWayScrew()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ATheWhiskyWayScrew::BeginPlay()
{
	Super::BeginPlay();

	//UE_LOG(LogTemp, Warning, TEXT("huhu!"));
	ATheWhiskyWayInteractable::SetupInteractableSpecs(false, Interactable::Screw);
}

void ATheWhiskyWayScrew::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
