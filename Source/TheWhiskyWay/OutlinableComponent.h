// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractableComponent.h"
#include "OutlinableComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THEWHISKYWAY_API UOutlinableComponent : public UInteractableComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOutlinableComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UTheWhiskyWayGameInstance* GameInstance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	bool bIsAffectedByAlcohol;

	UFUNCTION(BlueprintCallable, Category = "Game Events")
	void OnSpottedEventHandler(bool bIsInteractableSpotted, AActor* Actor, float NewAlcoholLevel);

	UFUNCTION(BlueprintCallable, Category = "Game Events")
	void OnPlayerNearMachineEventHandler(bool IsNearMachine, AActor* Actor, float AlcoholLevel);

	bool bIsSpotted;
	bool IsDrunk(float AlcoholLevel);
};
