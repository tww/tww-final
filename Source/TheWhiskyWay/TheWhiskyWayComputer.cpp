// Fill out your copyright notice in the Description page of Project Settings.

#include "TheWhiskyWay.h"
#include "TheWhiskyWayComputer.h"

ATheWhiskyWayComputer::ATheWhiskyWayComputer()
{
}

void ATheWhiskyWayComputer::BeginPlay()
{
	Super::BeginPlay();
	ATheWhiskyWayInteractable::SetupInteractableSpecs(false, Interactable::Computer);
	SetupCodeText(DefaultCodeText);
}

bool ATheWhiskyWayComputer::WasKeyAlreadyUsed(FString KeyDisplayName)
{
	for (int i = 0; i != KeysUsed.Num(); i++)
	{
		if (KeysUsed[i].Equals(KeyDisplayName))
		{
			return true;
		}
	}
	return false;
}

void ATheWhiskyWayComputer::ExtractFromString(FString string)
{
	SingleCodeText = "";
	if (string.Len() != 0)
	{
		for (int i = 0; i != string.Len(); i++)
		{
			if (string[i] == '/')
			{
				string.RemoveAt(0, i + 1);
				LeftCodeText = string;
				break;
			}
			else
			{
				SingleCodeText.AppendChar(string[i]);
			}
		}
	}

}

void ATheWhiskyWayComputer::SetupCodeText(FString stringDefault)
{
	LeftCodeText.Append(stringDefault);
}


