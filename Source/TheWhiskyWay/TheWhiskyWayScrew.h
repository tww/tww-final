// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TheWhiskyWayInteractable.h"
#include "TheWhiskyWayScrew.generated.h"

/**
 * 
 */
UCLASS()
class THEWHISKYWAY_API ATheWhiskyWayScrew : public ATheWhiskyWayInteractable
{
	GENERATED_BODY()
	
public:
	ATheWhiskyWayScrew();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
};
