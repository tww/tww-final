// Fill out your copyright notice in the Description page of Project Settings.

#include "TheWhiskyWay.h"
#include "TheWhiskyWayMultitool.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

// Sets default values
ATheWhiskyWayMultitool::ATheWhiskyWayMultitool()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ATheWhiskyWayMultitool::BeginPlay()
{
	Super::BeginPlay();

	bIsToolDocked = false;
}

// Called every frame
void ATheWhiskyWayMultitool::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}




