// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TheWhiskyWayGameInstance.h"
#include "CoreMinimal.h"
#include "InteractableComponent.h"
#include "FocusableComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THEWHISKYWAY_API UFocusableComponent : public UInteractableComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFocusableComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UTheWhiskyWayGameInstance* GameInstance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	bool bIsAffectedByAlcohol;

	UFUNCTION(BlueprintCallable, Category = "Player Input Events")
	void OnInteractionInputEventHandler(bool IsFocusingInteractable, AActor* Actor, float NewAlcoholLevel);

	bool IsDrunk(float AlcoholLevel);

	

	float CameraFadeDuration;

	FTimerHandle CameraBlendTimerHandle;

	void CallEventBlendAfterDelay();
	
};
