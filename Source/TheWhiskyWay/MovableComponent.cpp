// Fill out your copyright notice in the Description page of Project Settings.

#include "TheWhiskyWay.h"
#include "MovableComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

// Sets default values for this component's properties
UMovableComponent::UMovableComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UMovableComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	if (GetWorld()->GetGameInstance())
	{
		GameInstance = Cast<UTheWhiskyWayGameInstance>(GetWorld()->GetGameInstance());
		GameInstance->GlobalEventHandler->OnPlayerRepairEvent.AddDynamic(this, &UMovableComponent::OnInputActionBindingEventHandler);
		GameInstance->GlobalEventHandler->OnMultitoolDockedEvent.AddDynamic(this, &UMovableComponent::OnMultitoolDockedEventHandler);
	}

	TrackingTime = 0.05f; //measured 0.05 is perfect time for movement measuring
}


// Called every frame
void UMovableComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bIsPlayerRepairing)
	{
		TrackMousePosition();
		if (!Cast<ATheWhiskyWayInteractable>(GetOwner())->bIsInteractableFixed && MovingSpeed > 0)
		{
			TightenInteractable(DeltaTime);
		}
	}
}

void UMovableComponent::OnInputActionBindingEventHandler(AActor* Actor, bool isRepairing, float AlcoholLevel)
{
	if (Actor == GetOwner() && isRepairing)
	{
		if ((bIsAffectedByMultitool && bIsMultitoolDocked) || !bIsAffectedByMultitool)
		{
			bIsPlayerRepairing = isRepairing;
			CheckMouseMovement();
		}
	}
	else if (!isRepairing)
	{
		bIsPlayerRepairing = isRepairing;

		if (bIsMultitoolDocked && !Cast<ATheWhiskyWayInteractable>(GetOwner())->bIsInteractableFixed)
		{
			ResetComponent();
		}
	}
}

void UMovableComponent::OnMultitoolDockedEventHandler(AActor* Actor)
{
	if (Actor == GetOwner())
	{
		bIsMultitoolDocked = true;
	}
}

void UMovableComponent::TrackMousePosition()
{
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetMousePosition(MousePosition.X, MousePosition.Y);
	SaveMousePosition.Add(MousePosition);
}

void UMovableComponent::CheckMouseMovement()
{
	if (SaveMousePosition.Num() > 1)
	{
		FVector2D FirstVector = SaveMousePosition[0];
		FVector2D LastVector = SaveMousePosition[SaveMousePosition.Num() - 1];

		SaveMousePosition.Empty();

		float distance = LastVector.Y - FirstVector.Y;
		if (distance > 0) //mouse was moved down
		{
			MovingSpeed = distance;
		}
		else
		{
			MovingSpeed = 0.f;
		}
	}
	if (bIsPlayerRepairing)
	{
		GetWorld()->GetTimerManager().SetTimer(PositionLoggerTimerHandle, this, &UMovableComponent::CheckMouseMovement, TrackingTime, false);
	}
}

void UMovableComponent::TightenInteractable(float DeltaTime)
{
	if (Cast<ATheWhiskyWayInteractable>(GetOwner())->InteractableMesh->GetComponentRotation().Roll < Cast<ATheWhiskyWayInteractable>(GetOwner())->OriginRotation.Roll + Rotation)
	{
		Cast<ATheWhiskyWayInteractable>(GetOwner())->InteractableMesh->AddLocalRotation(FRotator(.0f, .0f, DeltaTime * MovingSpeed * 1.5f));
	}
	else
	{
		Cast<ATheWhiskyWayInteractable>(GetOwner())->bIsInteractableFixed = true;
		ResetComponent();
		GameInstance->GlobalEventHandler->OnInteractableFixedEvent.Broadcast(GetOwner());
	}
}

void UMovableComponent::ResetComponent()
{
	bIsMultitoolDocked = false;
	bIsPlayerRepairing = false;
}

