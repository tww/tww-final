// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TheWhiskyWayInteractable.h"
#include "TheWhiskyWayEnums.h"
#include "TheWhiskyWayGlobalEventHandler.h"
#include "TheWhiskyWayGameInstance.h"
#include "InteractableComponent.generated.h"


UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THEWHISKYWAY_API UInteractableComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInteractableComponent();

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interactable Component")
    UTheWhiskyWayGlobalEventHandler* GlobalEventHandler;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interactable Component")
	ATheWhiskyWayInteractable* OwningInteractable;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
