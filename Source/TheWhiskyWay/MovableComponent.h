// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TheWhiskyWayGameInstance.h"
#include "TheWhiskyWayInteractable.h"
#include "CoreMinimal.h"
#include "InteractableComponent.h"
#include "MovableComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THEWHISKYWAY_API UMovableComponent : public UInteractableComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMovableComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Events")
	UTheWhiskyWayGameInstance* GameInstance;

	UFUNCTION(BlueprintCallable, Category = "Player Input Events")
	void OnInputActionBindingEventHandler(AActor* Actor, bool isRepairing, float AlcoholLevel);

	UFUNCTION(BlueprintCallable, Category = "Player Input Events")
	void OnMultitoolDockedEventHandler(AActor* Actor);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	float Rotation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	bool bIsAffectedByMultitool;

	float TrackingTime;
	float MovingSpeed;
	float MaxRoation;
	bool bIsPlayerRepairing;
	bool bIsMultitoolDocked;
	FVector2D MousePosition;
	TArray<FVector2D> SaveMousePosition;
	FTimerHandle PositionLoggerTimerHandle;

	void TrackMousePosition();
	void TightenInteractable(float DeltaTime);
	void CheckMouseMovement();
	void ResetComponent();
	
};
