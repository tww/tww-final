// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TheWhiskyWayInteractable.h"
#include "TheWhiskyWayComputer.generated.h"

/**
 * 
 */
UCLASS()
class THEWHISKYWAY_API ATheWhiskyWayComputer : public ATheWhiskyWayInteractable
{
	GENERATED_BODY()
	
public:
	ATheWhiskyWayComputer();

	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
		TArray<FString> KeysUsed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
		FString DefaultCodeText = "class /Heat/Disperser/Controller() /implements /Air/Conditioner(){\n/   Num /max/Temperature = /40;\n/   Num /min/Temperature = 20;\n/   Void /Inspect/Room/Heat(){\n/      if(/!Is/Temperature/In/Scope(/max/Temperature, /min/Temperature)){\n/         Start/Air/Conditioning/System(/Get/Actual/Temperature(), /max/Temperature, /min/Temperature);\n/      }\n/   }\n/   Bool /Is/Temperature/In/Scope(/Num /max/Num, /Num /minNum){\n/      if(/Get/Actual/Temperature() > /maxNum || /Get/Actual/Temperature() < /min/Num){\n/         return /false;\n/      }else{\n/         return /true;\n/      }\n/   }\n/   Num /Get/Actual/Temperature(){\n/      return /Air/Conditioner./get/Room/Temperature();\n/   }\n/";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
		FString DefaultCodeText2 = "   Void /Start/Air/Conditioning/System(/Num /actual/Temperature, /Num /max/Temperature, /Num /min/Temperature){\n/      Num /temperature/Difference;\n/      if(/actual/Temperature > /max/Temperature){\n/         temperature/Difference = /actual /Temperature - /max/Temperature;\n/         Air/Conditioner./Down/Regulate/Temperature(/temperature/Difference);\n/      }else /if(/actual /Temperature < /min/Temperature){\n/         temperature/Difference = /min/Temperature - /actual/ emperature;\n/         Air/Conditioner./Up/Regulate/Temperature(/temperature/Difference);\n/      }\n/   }\n/}/";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
		FString LeftCodeText = "";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
		FString SingleCodeText = "";

	UFUNCTION(BlueprintCallable, Category = "Interaction")
		bool WasKeyAlreadyUsed(FString KeyDisplayName);

	UFUNCTION(BlueprintCallable, Category = "Interaction")
		void ExtractFromString(FString string);

	UFUNCTION(BlueprintCallable, Category = "Interaction")
		void SetupCodeText(FString stringDefault);
	
};
