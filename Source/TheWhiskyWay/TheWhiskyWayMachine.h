// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TheWhiskyWayInteractable.h"
#include "TheWhiskyWayGameInstance.h"
#include "GameFramework/Actor.h"
#include "TheWhiskyWayEnums.h"
#include "AkAudioDevice.h"
#include "AkAudioEvent.h"
#include "AkAudioClasses.h"
#include "TheWhiskyWayMachine.generated.h"

UCLASS()
class THEWHISKYWAY_API ATheWhiskyWayMachine : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATheWhiskyWayMachine();

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
        MachineName CurrentMachineName;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
        MachineState CurrentMachineState;

    UPROPERTY(EditAnywhere)
        USceneComponent* MachineRoot;

    UPROPERTY(EditAnywhere)
        UShapeComponent* MachineTriggerSphere;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Events")
		UTheWhiskyWayGameInstance* GameInstance;

	FTimerHandle MachineBreakTimerHandle;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
        float AlcoholLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
		float MaxBreakDelay;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
		float MinBreakDelay;

    static int BrokenCount;

    static int QuickFixCount;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "The Whisky Way|Audio")
        UAkAudioEvent* EventBroken;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "The Whisky Way|Audio")
        UAkAudioEvent* EventQuickFixed;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "The Whisky Way|Audio")
        UAkAudioEvent* EventFixed;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "The Whisky Way|Audio")
        UAkAudioEvent* RunningGag;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "The Whisky Way|Audio")
        UAkAudioEvent* RunningGagResponseNormal;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "The Whisky Way|Audio")
        UAkAudioEvent* RunningGagResponseAnnoyed;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "The Whisky Way|Audio")
        UAkAudioEvent* RunningGagResponseRAGE;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "The Whisky Way|Audio")
        UAkAudioEvent* StuckInQuickFix;

    // Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
    virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;

	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

    void SetupComponents();

    UFUNCTION(BlueprintCallable, Category = "The Whisky Way|Game Events")
    void OnChangeMachineStateEventHandler(MachineName MachineName, MachineState newMachineState, bool PlayDialogue);

	UFUNCTION(BlueprintCallable, Category = "Interaction")
	void BreakMachine();
	
	void BreakMachineAfterDelay();

	UFUNCTION(BlueprintCallable, Category = "Interaction")
	float CalculateDelayOnAlcoholLevel();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction")
	bool bIsFirstBreak;
};
