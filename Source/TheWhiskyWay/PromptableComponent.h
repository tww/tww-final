// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TheWhiskyWayEnums.h"
#include "CoreMinimal.h"
#include "InteractableComponent.h"
#include "PromptableComponent.generated.h"

/**
 * 
 */
UCLASS()
class THEWHISKYWAY_API UPromptableComponent : public UInteractableComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UPromptableComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	MachineName DependingMachine;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	FString PromptText;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	float PromptPositionX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	float PromptPositionY;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dependencies")
	bool bIsAffectedByAlcohol;
	
};
